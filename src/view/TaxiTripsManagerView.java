package view;

import java.text.ParseException;
import java.util.Scanner;

import javax.xml.transform.Source;

import controller.Controller;
import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.RedBlackBST;
import model.data_structures.hashTableLinearProbing;
import model.data_structures.hashTableSeparateChaining;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.GrupoServicioMi;
import model.vo.InfoTaxiRango;
import model.vo.InfoZonaRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.TaxiPuntos;
import model.vo.ZonaServicios;


/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada

			case 1: // cargar informacion a procesar

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;

				case 4:
					linkJson = TaxiTripsManager.D_PRUEVA;
					break;
				}

				System.out.println("Datos cargados: " + linkJson);
				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1B	
			case 2:	
				System.out.println(Controller.darInfoArbol());
				break;

			case 3:
				System.out.println("ingresar id");
				
				String id = sc.next();
				
//				InfoTaxiRango info = Controller.infoTaxi(id);
//				System.out.println("compani : "+info.getCompany()+"\n id :"+info.getIdTaxi()+"\n total dinero recibido : "+info.getPlataGanada()+"\nlel tiempo total en segundos es :"
//		jose 		+info.getTiempoTotal()+ "\n la distancia total recorrida en millas es : "+info.getDistanciaTotalRecorrida()+"\n el total de ervicios realizado es :"+info.getServiciosPrestadosEnRango().size());		
				
				break;
				
			case 4:
				System.out.println("ingresar id menor");
				String fecha = sc.next();
				System.out.println("ingresar id mayor");
				String hora = sc.next();				
				Cola <Service> c = Controller.idsEnRango(fecha, hora);
				if (c.isEmpty())
				{
					System.out.println("vacio");
				}
				while(!c.isEmpty())
				{
					System.out.println(c.dequeue());
				}
				break;
			
			

			
			case 9: 
				fin=true;
				sc.close();
				break;
			}

		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("------------------ISIS 1206 - Estructuras de datos----------");
		System.out.println("----------------------------Taller 4----------------------");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large) y generar un arreglo de con los servicios en rango.");
		System.out.println("2. Info del arbol red black");
		System.out.println("3. Info del taxi seg�n id");
		System.out.println("4. ids de los taxis en rango");
		System.out.println("9. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");


	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
