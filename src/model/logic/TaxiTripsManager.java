package model.logic;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.sound.midi.MidiDevice.Info;
import org.w3c.dom.stylesheets.LinkStyle;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import model.vo.Taxi;
import model.vo.TaxiPuntos;
import sun.applet.Main;
import sun.awt.util.IdentityLinkedList;
import model.vo.Compania;
import model.vo.CompaniaTaxi;
import model.vo.GrupoService;
import model.vo.GrupoServicioMi;
import model.vo.InfoZonaRango;
import model.vo.RangoFechaHora;
import model.vo.Service;

import model.data_structures.Arco;
import model.data_structures.ArcoYaExisteException;
import model.data_structures.Cola;
import model.data_structures.Coordenada;
import model.data_structures.DiGraph;
import model.data_structures.HashTreeDeServicios;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.Pila;
import model.data_structures.RedBlackBST;
import model.data_structures.Vertice;
import model.data_structures.VerticeNoExisteException;
import model.data_structures.VerticeYaExisteException;
import model.data_structures.hashTableLinearProbing;
import model.data_structures.hashTableSeparateChaining;

public class TaxiTripsManager <T extends Comparable<T>> {

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	public static final String D_PRUEVA = "./data/vvv.json";

	// TODO
	// Definition of data model 

	
	private Lista<Vertice> listaDeVertices = new Lista<Vertice>();
	private Cola<Service> colaDeServicios = new Cola<Service>();
	private DiGraph<String, Coordenada, Service> graf;

	public boolean cargarSistema(String direccionJson) throws ParseException 
	{		
		
		String compani = "Independent Owner";
		String dropoff_census_tract = "NaN";
		String dropoff_centroid_latitude  = "NaN";
		String dropoff_centroid_longitude = "NaN";
		JsonObject dropoff_localization_obj = null; 
		Lista<Double> dropoff_localization = new Lista<>();
		String dropoff_community_area = "NaN";
		String extras = "NaN";
		String fare = "NaN";
		String payment_type = "NaN";
		String pickup_census_tract = "NaN";
		String pickup_centroid_latitude  = "NaN";
		String pickup_centroid_longitude = "NaN";
		JsonObject pickup_localization_obj = null; 
		Lista<Double> pickup_localization = new Lista<>();
		String pickup_community_area = "NaN";
		String taxi_id = "NaN";
		String tips = "NaN";
		String tolls = "NaN";
		String trip_end_timestamp = "NaN";
		String trip_id = "NaN";
		String trip_miles = "NaN";
		String trip_seconds = "NaN";
		String trip_start_timestamp = "NaN";
		String trip_total = "NaN";
		Coordenada ini = new Coordenada(0, 0);
		Coordenada fin = new Coordenada(0, 0);
		JsonParser parser = new JsonParser();

		try {


			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(direccionJson));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{

				JsonObject obj = (JsonObject) arr.get(i);


				if ( obj.get("company") != null ) {
					compani = obj.get("company").getAsString();}				
				//--------------------------------

				if ( obj.get("dropoff_census_tract") != null ) {
					dropoff_census_tract  = obj.get("dropoff_census_tract").getAsString();
				}

				if ( obj.get("dropoff_centroid_latitude") != null ) {
					dropoff_centroid_latitude  = obj.get("dropoff_centroid_latitude").getAsString();
				}			

				if ( obj.get("dropoff_centroid_longitude") != null )
				{
					dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); 
				}

				if ( obj.get("dropoff_centroid_location") != null )
				{
					dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();
					JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();				
					double longitude = dropoff_localization_arr.get(0).getAsDouble();
					double latitude = dropoff_localization_arr.get(1).getAsDouble();

					ini.setLatitud(latitude);	
					ini.setLongitud(longitude);
				}

				if ( obj.get("dropoff_community_area") != null ) {
					dropoff_community_area = obj.get("dropoff_community_area").getAsString();
				}

				//--------------------------------


				if ( obj.get("extras") != null ) {
					extras = obj.get("extras").getAsString();
				}
				if ( obj.get("fare") != null ) {
					fare = obj.get("fare").getAsString();
				}
				if ( obj.get("payment_type") != null ) {
					payment_type = obj.get("payment_type").getAsString();
				}

				//--------------------------------

				if ( obj.get("pickup_census_tract") != null ) {
					pickup_census_tract  = obj.get("pickup_census_tract").getAsString();
				}

				if ( obj.get("pickup_centroid_latitude") != null ) {
					pickup_centroid_latitude  = obj.get("pickup_centroid_latitude").getAsString();
				}			

				if ( obj.get("pickup_centroid_longitude") != null ){
					pickup_centroid_longitude = obj.get("pickup_centroid_longitude").getAsString(); 
				}

				if ( obj.get("pickup_centroid_location") != null )
				{ 
					pickup_localization_obj =obj.get("pickup_centroid_location").getAsJsonObject();
					JsonArray pickup_localization_arr = pickup_localization_obj.get("coordinates").getAsJsonArray();				
					double longitud= pickup_localization_arr.get(0).getAsDouble();
					double latitud = pickup_localization_arr.get(1).getAsDouble();

					fin.setLatitud(latitud);	
					fin.setLongitud(longitud);
				}

				if ( obj.get("pickup_community_area") != null ){
					pickup_community_area = obj.get("pickup_community_area").getAsString();
				}

				//----------
				if ( obj.get("taxi_id") != null ){
					taxi_id = obj.get("taxi_id").getAsString();
				}
				if ( obj.get("tips") != null ){
					tips= obj.get("tips").getAsString();
				}
				if ( obj.get("tolls") != null ){
					tolls = obj.get("tolls").getAsString();
				}
				if ( obj.get("trip_end_timestamp") != null ){
					trip_end_timestamp = obj.get("trip_end_timestamp").getAsString();
				}
				if ( obj.get("trip_id") != null ){
					trip_id = obj.get("trip_id").getAsString();
				}
				if ( obj.get("trip_miles") != null ){
					trip_miles = obj.get("trip_miles").getAsString();
				}
				if ( obj.get("trip_seconds") != null ){
					trip_seconds = obj.get("trip_seconds").getAsString();
				}
				if ( obj.get("trip_start_timestamp") != null ){
					trip_start_timestamp = obj.get("trip_start_timestamp").getAsString();
				}
				if ( obj.get("trip_total") != null ){
					trip_total = obj.get("trip_total").getAsString();
				}
				//				System.out.println("long ini "+ini.getLongitud()+"+"+ini.getLatitud()+"+"+fin.getLongitud()+"+"+fin.getLatitud());
				Service servicio = new Service(compani, dropoff_census_tract, dropoff_centroid_latitude, fin, dropoff_centroid_longitude, dropoff_community_area, extras, fare, payment_type, pickup_census_tract, pickup_centroid_latitude, ini, pickup_centroid_longitude, pickup_community_area, taxi_id, tips, tolls, trip_end_timestamp, trip_id, trip_miles, trip_seconds, trip_start_timestamp, trip_total);
				fin = servicio.getDropoff_centroid_location();
				ini = servicio.getPickup_centroid_location();
				//				System.out.println("long ini "+ini.getLongitud()+"+"+ini.getLatitud()+"+"+fin.getLongitud()+"+"+fin.getLatitud()+"-.-.-.-\n"+servicio.getTripId());

				colaDeServicios.enqueue(servicio);	



			}


		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return false;
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
			return false;
		}
		System.out.println(colaDeServicios.size());
		crearGrafo(100);
		cargarGrafo();
		return true;
	}	



	public void crearGrafo(int pDistancia) 
	{
		boolean enControIni = false;
		boolean enControFin = false;
		Vertice <String, Coordenada, Service> iniV =null;
		Vertice <String, Coordenada, Service> finV =null;
		double disIniM = -1;
		double disFinM = -1;
		graf = new DiGraph<String, Coordenada, Service>();
		System.out.println(listaDeVertices.size()+"hola");	
		Service ser =null;
		while(!colaDeServicios.isEmpty())			
		{
			ser = colaDeServicios.dequeue();	
			//			Coordenada fin = ser.getDropoff_centroid_location();
			//			Coordenada ini = ser.getPickup_centroid_location();
			Coordenada fin = new Coordenada(ser.getDropoff_centroid_latitude(), ser.getDropoff_centroid_longitude());
			Coordenada ini = new Coordenada(ser.getPickup_centroid_latitude(), ser.getPickup_centroid_longitude());
			//			System.out.println(ser.getPickup_centroid_longitude()+"+"+ser.getPickup_centroid_latitude()+"+"+ser.getDropoff_centroid_longitude()+"+"+ser.getDropoff_centroid_latitude());
			//			System.out.println("long ini "+ini.getLongitud()+"+"+ini.getLatitud()+"+"+fin.getLongitud()+"+"+fin.getLatitud()+"+"+con);
			//			con++;
			if(ini.getLongitud()==0||ini.getLatitud()==0||fin.getLongitud()==0||fin.getLatitud()==0)
			{
				System.out.println("long ini "+ini.getLongitud()+"+"+ini.getLatitud()+"+"+fin.getLongitud()+"+"+fin.getLatitud());
				continue;
			}

			if(listaDeVertices.size()==0)
			{

				Vertice <String, Coordenada, Service> a = new Vertice<String, Coordenada, Service>(ini);
				Vertice <String, Coordenada, Service> b = new Vertice<String, Coordenada, Service>(fin);
				//				System.out.println("hola");
				try {

					graf.agregarVertice(ini);
					graf.agregarVertice(fin);
					graf.agregarArco(ini.darId() , fin.darId(), ser);
				} catch (VerticeNoExisteException | ArcoYaExisteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (VerticeYaExisteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				listaDeVertices.add(a);
				listaDeVertices.add(b);
				System.out.println(graf.darNArcos()+"andabpdigan+"+graf.darVertices().size()+"+"+listaDeVertices.size());				
			}
			else
			{

				listaDeVertices.listing();

				Vertice ac = listaDeVertices.getCurrent();
				while(listaDeVertices.hasNext())
				{
					Coordenada co = (Coordenada) ac.darInfoVertice();
					double laIA = ini.getLatitud();
					double loIA = ini.getLongitud();

					double laFA = fin.getLatitud();
					double loFA = fin.getLongitud();

					double laR = co.getLatitud();
					double loR = co.getLongitud();

					if(getDistance(laIA, loIA, laR, loR)<=pDistancia)
					{
						enControIni = true;
						double dis = getDistance(laIA, loIA, laR, loR);
						if (dis< disIniM || disIniM == -1)
						{
							iniV = ac;
							disIniM = dis;
						}
					}

					if(getDistance(laFA, loFA, laR, loR)<=pDistancia)
					{
						enControFin = true;
						double dis = getDistance(laFA, loFA, laR, loR);
						if (dis< disFinM || disFinM == -1)
						{
							finV = ac;
							disFinM = dis;
						}
					}

					ac=listaDeVertices.next();

				}
				if(enControFin&&enControIni)
				{
					try {
						graf.agregarArco(iniV.darId(), finV.darId(), ser);
					} catch (VerticeNoExisteException e) {
						// TODO Auto-generated catch block
						// 						System.out.println("el V existe");
					} catch (ArcoYaExisteException e) {
						// TODO Auto-generated catch block
						// 						System.out.println("el A existe");
					}
				}
				else if(!enControFin&&enControIni)
				{
					Vertice <String, Coordenada, Service> b = new Vertice<String, Coordenada, Service>(fin);
					try {
						graf.agregarVertice(fin);
						graf.agregarArco(iniV.darId() , fin.darId(), ser);
					} catch (VerticeYaExisteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (VerticeNoExisteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ArcoYaExisteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("no fin");
					listaDeVertices.add(b);
				}


				else if(enControFin&&!enControIni)
				{
					Vertice <String, Coordenada, Service> a = new Vertice<String, Coordenada, Service>(ini);
					try {
						graf.agregarVertice(ini);
						graf.agregarArco(ini.darId() , finV.darId(), ser);
					} catch (VerticeYaExisteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (VerticeNoExisteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ArcoYaExisteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("no ini");
					listaDeVertices.add(a);
				}
				else if(!enControIni&&!enControFin)
				{
					Vertice <String, Coordenada, Service> a = new Vertice<String, Coordenada, Service>(ini);
					Vertice <String, Coordenada, Service> b = new Vertice<String, Coordenada, Service>(fin);

					try {

						graf.agregarVertice(ini);
						graf.agregarVertice(fin);
						graf.agregarArco(ini.darId() , fin.darId(), ser);
					} catch (VerticeNoExisteException | ArcoYaExisteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (VerticeYaExisteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("no ini");
					System.out.println("no fini");
					listaDeVertices.add(a);
					listaDeVertices.add(b);

				}

			}


			enControIni = false;
			enControFin = false;
		}
		System.out.println("num arcos "+graf.darNArcos()+"\nnum vertices "+graf.darVertices().size());
		try {
			guardarGrafo();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth in meters
		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(toRad(lat1))
		* Math.cos(toRad(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		// System.out.println(distance);
		return distance;
	}

	public Double toRad(double d)
	{
		return (d*Math.PI)/180;
	}


	public void guardarGrafo() throws IOException
	{
		JsonWriter writer = new JsonWriter(new FileWriter("./data/gr.json"));

		Collection<Vertice<String, Coordenada, Service>> listaVertices=graf.darObjetosVertices();

		//Empieza la lista de vertices 
		writer.beginArray();
		Vertice<String, Coordenada, Service> cor = null;
		Iterator ite = listaVertices.iterator();
		while ( ite.hasNext())
		{
			cor = (Vertice<String, Coordenada, Service>) ite.next();
			writer.beginObject();
			Coordenada verActual= cor.darInfoVertice();
			//empieza el objeto vertice
			writer.name("id");
			writer.value(verActual.darId()+"");
			writer.name("longitud");
			writer.value(verActual.getLongitud());
			writer.name("latitud");
			writer.value(verActual.getLatitud());

			//Empieza la lista de arcos salientes
			writer.name("arcosSalientes");
			ArrayList<Arco<String, Coordenada, Service>> listaArcS=cor.darSucesores();
			System.out.println(listaArcS.size()+ "Size of list");
			writer.beginArray();
			for (int i=0; i<listaArcS.size(); i++ )
			{
				Arco<String, Coordenada, Service> arcActual=listaArcS.get(i);
				writer.beginObject();
				writer.name("llegada");
				writer.value(arcActual.darVerticeDestino().darId()+"");
				writer.name("salida");
				writer.value(arcActual.darVerticeOrigen().darId()+"");					
				Service serAsociado= arcActual.darInfoArco();

				writer.name("trip_id");
				writer.value(serAsociado.getTripId());
				writer.name("taxi_id");
				writer.value(serAsociado.getTaxiId());
				writer.name("trip_seconds");
				writer.value(serAsociado.getTripSeconds()+"");
				writer.name("trip_miles");
				writer.value(serAsociado.getTripMiles()+"");
				writer.name("trip_total");
				writer.value(serAsociado.getTripTotal()+"");
				writer.name("dropoff_community_area");
				writer.value(serAsociado.getDropoffZone()+"");
				writer.name("trip_start_timestamp");
				writer.value(serAsociado.getTripStart()+"");
				writer.name("trip_end_timestamp");
				writer.value(serAsociado.getTripEnd()+"");
				writer.name("company");
				if(serAsociado.getTaxi().getCompany()!=null)
				{
					writer.value(serAsociado.getTaxi().getCompany());
				}
				else
				{
					writer.value("Independent Owner");
				}
				writer.name("dropoff_centroid_latitude");
				writer.value(serAsociado.getDropoff_centroid_latitude()+"");
				writer.name("pickup_centroid_latitude");
				writer.value(serAsociado.getPickup_centroid_latitude()+"");
				writer.name("dropoff_centroid_longitude");
				writer.value(serAsociado.getDropoff_centroid_longitude()+"");
				writer.name("pickup_centroid_longitude");
				writer.value(serAsociado.getPickup_centroid_longitude()+"");
				writer.name("distancia");
				writer.value(serAsociado.getTripMiles()+"");
				writer.name("pickup_community_area");
				writer.value(serAsociado.getPickuZone()+"");
				writer.name("fare");
				writer.value(serAsociado.getFare()+"");
				writer.name("tolls");
				writer.value(serAsociado.getTolls()+"");
				writer.endObject();



			}
			writer.endArray();

			//Empieza la lista de arcos entrantes 
			writer.name("arcosEntrantes");
			ArrayList<Arco<String, Coordenada, Service>> listaArcE=cor.darPredecesores();
			System.out.println(listaArcE.size() + "en");
			writer.beginArray();

			for (int i=0; i<listaArcE.size(); i++ )
			{
				Arco<String, Coordenada, Service> arcActual=listaArcE.get(i);
				writer.beginObject();
				writer.name("llegada");
				writer.value(arcActual.darVerticeDestino().darId()+"");
				writer.name("salida");
				writer.value(arcActual.darVerticeOrigen().darId()+"");

				Service serAsociado= arcActual.darInfoArco();				
				writer.name("trip_id");
				writer.value(serAsociado.getTripId());
				writer.name("taxi_id");
				writer.value(serAsociado.getTaxiId());
				writer.name("trip_seconds");
				writer.value(serAsociado.getTripSeconds()+"");
				writer.name("trip_miles");
				writer.value(serAsociado.getTripMiles()+"");
				writer.name("trip_total");
				writer.value(serAsociado.getTripTotal()+"");
				writer.name("dropoff_community_area");
				writer.value(serAsociado.getDropoffZone()+"");
				writer.name("trip_start_timestamp");
				writer.value(serAsociado.getTripStart()+"");
				writer.name("trip_end_timestamp");
				writer.value(serAsociado.getTripEnd()+"");
				writer.name("company");
				if(serAsociado.getTaxi().getCompany()!=null)
				{
					writer.value(serAsociado.getTaxi().getCompany());
				}
				else
				{
					writer.value("Independent Owner");
				}
				writer.name("dropoff_centroid_latitude");
				writer.value(serAsociado.getDropoff_centroid_latitude()+"");
				writer.name("pickup_centroid_latitude");
				writer.value(serAsociado.getPickup_centroid_latitude()+"");
				writer.name("dropoff_centroid_longitude");
				writer.value(serAsociado.getDropoff_centroid_longitude()+"");
				writer.name("pickup_centroid_longitude");
				writer.value(serAsociado.getPickup_centroid_longitude()+"");
				writer.name("distancia");
				writer.value(serAsociado.getTripMiles()+"");
				writer.name("pickup_community_area");
				writer.value(serAsociado.getPickuZone()+"");
				writer.name("fare");
				writer.value(serAsociado.getFare()+"");
				writer.name("tolls");
				writer.value(serAsociado.getTolls()+"");
				writer.endObject();


			}
			writer.endArray();	
			writer.endObject();
		}
		writer.endArray();
		writer.close();
	}


	public void cargarGrafo() 
	{
		System.out.println("entro al cargar");
		Coordenada idVertice=null;
		Service oebjVertice=null;
		String compani = "Independent Owner";
		String dropoff_census_tract = "NaN";
		String dropoff_centroid_latitude  = "NaN";
		String dropoff_centroid_longitude = "NaN";
		JsonObject dropoff_localization_serActual = null; 
		Coordenada dropoff_localization = new Coordenada(0, 0);
		String dropoff_community_area = "NaN";
		String extras = "NaN";
		double fare = -1;
		String payment_type = "NaN";
		String pickup_census_tract = "NaN";
		String pickup_centroid_latitude  = "NaN";
		String pickup_centroid_longitude = "NaN";
		JsonObject pickup_localization_serActual = null; 
		Coordenada pickUp_localization =  new Coordenada(0, 0);
		int pickup_community_area = -1;
		String taxi_id = "NaN";
		String tips = "NaN";
		String tolls = "NaN";
		String trip_end_timestamp = "NaN";
		String trip_id = "NaN";
		double trip_miles = -1;
		int trip_seconds =-1;
		String trip_start_timestamp = "NaN";
		String trip_total = "NaN";


		DiGraph<String, Coordenada, Service> grafito = new DiGraph<String, Coordenada, Service>();
		JsonParser parser = new JsonParser();

		try {


			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader("./data/gr.json"));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{

				JsonObject obj = (JsonObject) arr.get(i);
				if ( obj.get("id") != null ) 
				{
					System.out.println("id no es nulo");
					String coor[] = obj.get("id").getAsString().split("/");
					System.out.println(coor[0]+ " la coordenada en 0");
					idVertice= new Coordenada(Double.parseDouble(coor[0]), Double.parseDouble(coor[1]));
					System.out.println(idVertice + " coordenada");
				} 


				// Se crea el vertice 
				System.out.println(" imprimo");
				try
				{ System.out.println("lo intenta");
					grafito.agregarVertice(idVertice);	
					System.out.println("lo logra");
				}
				catch (VerticeYaExisteException e)
				{
					// TODO: handle exception
					System.out.println("no lo logra");
				}

				JsonArray arcosS= obj.get("arcosSalientes").getAsJsonArray();
				ArrayList<Arco<String, Coordenada, Service>> listaArcS= new ArrayList<Arco<String, Coordenada, Service>>();
				for(int j=0; j<arcosS.size();j++)
				{
					JsonObject arcoActual= arcosS.get(j).getAsJsonObject();
					if(arcoActual!=null)
					{
						System.out.println("Entra a arco ");
						String[] coorLle=arcoActual.get("llegada"
								).getAsString().split("/");
						System.out.println("crea cordenada ");
						Coordenada coorL= new Coordenada(Double.parseDouble(coorLle[0]), Double.parseDouble(coorLle[1]));
						String[] coorSal=arcoActual.get("salida").getAsString().split("/");
						Coordenada coorS= new Coordenada(Double.parseDouble(coorSal[0]), Double.parseDouble(coorSal[1]));
						Service ser = new Service(arcoActual.get("company").getAsString(), "NaN", arcoActual.get("dropoff_centroid_latitude").getAsString(), 
								new Coordenada(arcoActual.get("dropoff_centroid_latitude").getAsDouble(), arcoActual.get("dropoff_centroid_longitude").getAsDouble()), 
								arcoActual.get("dropoff_centroid_longitude").getAsString(),arcoActual.get("dropoff_community_area").getAsString(),	"NaNextras", 
								arcoActual.get("fare").getAsString(),"NAN", "NAN", arcoActual.get("pickup_centroid_latitude").getAsString(), 
								new Coordenada(arcoActual.get("pickup_centroid_latitude").getAsDouble(), arcoActual.get("pickup_centroid_longitude").getAsDouble()),
								arcoActual.get("pickup_centroid_longitude").getAsString(), arcoActual.get("pickup_community_area").getAsString(), 
								arcoActual.get("taxi_id").getAsString() , "nAN",arcoActual.get("tolls").getAsString() , arcoActual.get("trip_end_timestamp").getAsString(), arcoActual.get("trip_id").getAsString(),
								arcoActual.get("trip_miles").getAsString(), arcoActual.get("trip_seconds").getAsString(), arcoActual.get("trip_start_timestamp").getAsString(),
								arcoActual.get("trip_total").getAsString());
						try
						{
							grafito.agregarArco(coorS.darId(), coorL.darId(), ser);
						}
						catch (VerticeNoExisteException e) 
						{
							// TODO: handle exception
							grafito.agregarVertice(coorL);
							System.out.println("Crea vertice ");
							grafito.agregarArco(coorS.darId(), coorL.darId(), ser);
						}
					}
				}

				JsonArray arcosE= obj.get("arcosEntrantes").getAsJsonArray();
				ArrayList<Arco<String, Coordenada, Service>> listaArcE=new ArrayList<Arco<String, Coordenada, Service>>();
				for(int j=0; j<arcosE.size();j++)
				{
					System.out.println("Entra a arco 1");
					JsonObject arcoActual= arcosE.get(j).getAsJsonObject();
					if (arcoActual!=null)
					{
						System.out.println("lo intent}}}}");
						String[] coorLle=arcoActual.get("llegada").getAsString().split("/");
						Coordenada coorL= new Coordenada(Double.parseDouble(coorLle[0]), Double.parseDouble(coorLle[1]));
						String[] coorSal=arcoActual.get("salida").getAsString().split("/");
						Coordenada coorS= new Coordenada(Double.parseDouble(coorSal[0]), Double.parseDouble(coorSal[1]));
						Service ser = new Service(arcoActual.get("company").getAsString(), "NaN", arcoActual.get("dropoff_centroid_latitude").getAsString(), 
								new Coordenada(arcoActual.get("dropoff_centroid_latitude").getAsDouble(), arcoActual.get("dropoff_centroid_longitude").getAsDouble()), 
								arcoActual.get("dropoff_centroid_longitude").getAsString(),arcoActual.get("dropoff_community_area").getAsString(),	"NaNextras", 
								arcoActual.get("fare").getAsString(),"NAN", "NAN", arcoActual.get("pickup_centroid_latitude").getAsString(), 
								new Coordenada(arcoActual.get("pickup_centroid_latitude").getAsDouble(), arcoActual.get("pickup_centroid_longitude").getAsDouble()),
								arcoActual.get("pickup_centroid_longitude").getAsString(), arcoActual.get("pickup_community_area").getAsString(), 
								arcoActual.get("taxi_id").getAsString() , "nAN",arcoActual.get("tolls").getAsString() , arcoActual.get("trip_end_timestamp").getAsString(), arcoActual.get("trip_id").getAsString(),
								arcoActual.get("trip_miles").getAsString(), arcoActual.get("trip_seconds").getAsString(), arcoActual.get("trip_start_timestamp").getAsString(),
								arcoActual.get("trip_total").getAsString());
						System.out.println("asdfghjkl");
						try
						{
							grafito.agregarArco(coorS.darId(), coorL.darId(), ser);
						}
						catch (VerticeNoExisteException e) 
						{
							// TODO: handle exception
							grafito.agregarVertice(coorS);
							grafito.agregarArco(coorS.darId(), coorL.darId(), ser);
						}
					}

				}			


			}


		}

		catch(Exception e)
		{

		}

		System.out.println(grafito.darNArcos()+ " tama�o del grafo guardado");

	}



}
