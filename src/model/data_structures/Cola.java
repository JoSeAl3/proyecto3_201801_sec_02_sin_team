package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Cola< T > implements IQueue<T> ,Iterable<T>
{

	private Node first;
	private Node end;
	private int size;

	public Cola() 
	{
		end = null;
		first = null;
		size = 0;
	}

	public void enqueue(T o) {
		Node oldlast = end;
		end = new Node(o, null);       

		if (isEmpty()) 
			first = end;
		else           
		{
			oldlast.cambiarAnterior(end); 

		}
		size++;
	} 

	public	T dequeue() {
		if (isEmpty())
		{
			return null;
		}
		else
		{
		T o = (T) first.darObjeto();
		first = first.darAnterior();
		size--;
		return o;	
		  
		}
	 
	} 

	public boolean isEmpty() {
		return first == null;
	}

	public int size() {
		return size;
	}

	public Object first() {
		if (first == null)
			return null;
		else
			return first.darObjeto();
	}
	
	  
	    public Iterator<T> iterator() 
	    {
	        return new ListIterator();  
	    }


	    private class ListIterator implements Iterator<T> {
	        private Node current = first;

	        public boolean hasNext()
	        {
	        	return current != null;            
	        	}
	        public void remove()      { 
	        	throw new UnsupportedOperationException();
	        	}

	        public T next() {
	            if (!hasNext()) 
	            	throw new NoSuchElementException();
	            T item = (T) current.darObjeto();
	            current = current.darAnterior(); 
	            return item;
	        }
	    }

}

