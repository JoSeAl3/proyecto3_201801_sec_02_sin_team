package model.data_structures;

import java.io.Serializable;

public interface IArco extends Serializable
{
   // -----------------------------------------------------------------
   // M�todos
   // -----------------------------------------------------------------

   /**
    * Devuelve el peso del arco
    * @return Peso del arco
    */
   public int darPeso( );
}

