package model.data_structures;

import java.util.Iterator;

public class hashTableSeparateChaining <Key  , Value> 
{
	private static final int INIT_CAPACITY = 4;

	private int n;                                
	private int m;                                
	private stNode<Key, Value>[] st; 


	public hashTableSeparateChaining()
	{
		this(INIT_CAPACITY);
	} 

	hashTableSeparateChaining(int m) 
	{
		this.m = m;
		st = (stNode<Key, Value>[]) new stNode[m];
		for (int i = 0; i < m; i++)
			st[i] = new stNode<Key, Value>();
	} 


	private void resize(int chains) 
	{
		hashTableSeparateChaining <Key, Value> temp = new hashTableSeparateChaining<Key, Value>(chains);
		for (int i = 0; i < m; i++) {
			for (Key key : st[i].keys()) {
				temp.put(key, st[i].get(key));
			}
		}
		this.m  = temp.m;
		this.n  = temp.n;
		this.st = temp.st;
	}

	
	private int hash(Key key) 
	{
		return (key.hashCode() & 0x7fffffff) % m;
	} 


	public int size() 
	{
		return m;
	} 

	public int cantidadLaves()
	{
		return n;
	}


	public boolean isEmpty() {
		return size() == 0;
	}


	public boolean contains(Key key) {
		if (key == null)
		{
			throw new IllegalArgumentException("argument to contains() is null");

		}
		return get(key) != null;
	} 


	public Value get(Key key) {
		if (key == null) 
		{ 
			throw new IllegalArgumentException("argument to get() is null");
		}

		int i = hash(key);
		return st[i].get(key);
	} 

	
	public void put(Key key, Value val) {
		if (key == null)
		{
			throw new IllegalArgumentException("first argument to put() is null");
		}
		if (val == null)
		{
			delete(key);
			return;
		}

	
		if (n >= 6*m) resize(2*m);

		int i = hash(key);
		if (!st[i].contains(key)) n++;
		st[i].put(key, val);
	} 


	public void delete(Key key) 
	{
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");

		int i = hash(key);
		if (st[i].contains(key)) n--;
		st[i].delete(key);


		if (m > INIT_CAPACITY && n <= 2*m) 
		{
			resize(m/2);
		}
	} 



	public Iterator<Key> keys()	
	{
		return new iteradorDeStC();		
	}

	public class iteradorDeStC<K , V> implements Iterator<K>
	{

		K[] ll ;
		int pos;

		public iteradorDeStC()
		{
			ll = (K[]) new Object [n];
			pos=0;

			for ( int i = 0 ; i < m ; i++)
			{
				stNode<Key, Value>  temp = st[i];
				for (Key llave : st[i].keys() )
				{
					ll[pos++] = (K) llave;
				}
			}

		}
		@Override
		public boolean hasNext() 
		{
			// TODO Auto-generated method stub
			return pos < ll.length;
		}

		@Override
		public K next() 
		{			
			if (!hasNext())
			{
				return null;
			}
			else
			{
				return ll[pos++];
			}
		}

	}
}
