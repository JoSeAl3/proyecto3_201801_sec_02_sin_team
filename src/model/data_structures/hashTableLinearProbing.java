package model.data_structures;

import java.util.Date;
import java.util.Iterator;

import model.vo.Service;

public class hashTableLinearProbing <K ,V>  
{
	private int n;           //cantidad de llaves en la tabla
	private int m;           //tama�o de la tabla
	private K[] llaves;      //llaves
	private V[] valores;   	 // valores
	private static final int INI = 13; //constante de tam inicial

	public hashTableLinearProbing()
	{
		this(INI);
	}
	
	public hashTableLinearProbing(int capacidad)
	{
		m = capacidad;
		n = 0;
		llaves = (K[]) new Object[m];

		valores = (V[]) new Object[m];

	}

	public int size() {
		return m;
	}
	
	public int cantidadLaves()
	{
		return n;
	}
	public void put( K llave, V valor)
	{
		if(llave == null || valor == null)
		{
			System.out.println("Error la llave o el valor son null");
			System.out.println(llave);
			System.out.println(valor);
		}
		int i;
		for (i = hash(llave); llaves[i] != null; i = (i+1) % m) 
		{
			if ((  llaves[i]).equals(llave)) 
			{
				valores[i] = valor;
				return;
			}
		}
		llaves[i] = llave;
		valores[i] = valor;
		n++;
		
	
		
		if(factorDeCarga()>=0.5)
		{
			
			reSize();
		}
	
	}


	public V get(K llave)
	{
		for (int i = hash(llave); llaves[i] != null; i = (i+1) % m)
			if (llaves[i].equals(llave))
			{
				return valores[i];
			}
		return null;
	}


	public void delete(K llave)	
	{
		if (!contains(llave)) return;

		// busco la llave
		int i = hash(llave);
		while (!(llave.equals(llaves[i]))) 
		{
			i = (i + 1) % m;
		}

		// elimino los valores
		llaves[i] = null;
		valores[i] = null;
		n--;
		reHash(i);	
	}


	public int hash(K llave)
	{
		int x = 0;
		x = (llave.hashCode() & 0x7fffffff) % m;
		return x;

	}

	public double factorDeCarga() 
	{

		double numer = n;
		double deno = m;
		double f = numer/deno;		
		return f;

	}

	public void reHash(int pos )
	{
		// re-ordeno el arreglo desde la pos
		int i = (pos + 1) % m;
		while (llaves[i] != null) 
		{
			// eliminar y re agrgar todosa las llaves en el arreglo
			//guardo llaves y valores
			K  keyToRehash = llaves[i];
			V valToRehash = valores[i];
			//los elimino del arreglo
			llaves[i] = null;
			valores[i] = null;
			n--;
			// los re incerto
			put(keyToRehash, valToRehash);
			i = (i + 1) % m;
		}
	}

	public void reSize()
	{		
		hashTableLinearProbing<K, V> temp = new hashTableLinearProbing<K, V>(m*2);
		for (int i = 0; i < m; i= i+1) {
			if (llaves[i] != null) {
				temp.put(llaves[i], valores[i]);
			}
		}
		this.llaves = temp.llaves;
		this.valores = temp.valores;
		m    = temp.m;	
	}
	public boolean contains(K llave) {
		if (llave == null)
		{
			throw new IllegalArgumentException("llave null");

		}
		return get(llave) != null;
	}



	  public Iterable<K> keys() 
	  {
	        Cola<K> queue = new Cola<K>();
	        for (int i = 0; i < m; i++)
	        {
	            if (llaves[i] != null) queue.enqueue(llaves[i]);
	        }
	        return queue;
	    }


}
