package model.data_structures;

import com.sun.javafx.iio.ImageFormatDescription.Signature;

public class Lista <T>
{
	private Node primero;
	private Node actual;

	private int cont ;
	private Node ultimo;



	public Lista()
	{
		primero = null;
		cont =0;
	}


	public void add(T objeto) 
	{
		if ( primero == null)
		{
			primero = new Node(objeto, null) ;
			actual = primero;
			ultimo = primero;
			cont++;
//			System.out.println("supuesto agreego");
		}
		else
		{
			Node agregado = new Node(objeto, null);
			actual= primero;
			primero= agregado;
			primero.cambiarSiguiente(actual);
			actual.cambiarAnterior(primero);
			actual= primero;
			cont++;
//			System.out.println("supuesto agreego2");
		}


	}
	public Node darUltimo() {
		return ultimo;
	}
	public void delete(T pObjeto) 
	{
		if ( primero == null)
		{
			System.out.println("No existen elementos en la lista");
		}
		else if ( buscar(pObjeto))
		{
			if (primero.darObjeto() == pObjeto && primero.darSiguiente() != null)
			{
				// El primer nodo apunta al siguiente.
				actual = primero.darSiguiente();
				actual.cambiarAnterior(null);
				primero = actual;

			} 		

			else if (primero.darObjeto() != pObjeto && primero.darSiguiente() != null)
			{
				// Crea ua copia de la lista.
				actual = primero;
				// Recorre la lista hasta llegar al nodo anterior 
				// al de referencia.
				while(actual.darObjeto() != pObjeto)
				{
					actual = actual.darSiguiente();
				}
				// Guarda el nodo siguiente del nodo a eliminar.

				if(actual.darSiguiente() != null)
				{
					if(actual.darSiguiente().darSiguiente() != null)
					{
						Node siguiente = actual.darSiguiente().darSiguiente();
						// Enlaza el nodo anterior al de eliminar con el 
						// sguiente despues de el.
						actual.cambiarSiguiente(siguiente);  
						siguiente.cambiarAnterior(actual);
					}
					else
					{
						actual.cambiarSiguiente(null);
					}
				}
				else
				{
					actual = actual.darAnterior();
					actual.cambiarSiguiente(null);
				}
			}
			else 
			{
				primero = null;
			}

			// Disminuye el contador de tama�o de la lista.
			cont--;
		}
		else
		{
			System.out.println("No existe el elemento en la lista");
		}
	}

	public T get(T pObjeto)
	{
		if (primero!=null) {
			actual = primero;
			while(actual.darObjeto() != pObjeto)
			{
				actual = actual.darSiguiente();
			}
			return  (T) actual.darObjeto() ;
		}
		else 
		{
			return null;
		}
	}


	public int size() {

		return cont;
	}


	public void listing() 
	{
		if(primero != null) {
			actual = primero;
		}
	}


	public T getCurrent() 
	{		
		if(actual!= null)
		{
			return (T) actual.darObjeto();
		}
		else
		{
			return null;
		}
	}


	public T next()
	{
		if ( actual.darSiguiente() != null)
		{

			actual =  actual.darSiguiente();
			return (T) actual.darObjeto();
		}
		else
		{
			System.out.println("no hay siguiente");
			return null;
		}
	}

	public boolean buscar(T buscado){

		actual = primero;	        
		boolean encontrado = false;	       
		while(actual != null && encontrado != true)
		{
			if (buscado == actual.darObjeto())
			{	               
				encontrado = true;
			}
			else{

				actual = actual.darSiguiente();
			}
		}	       
		return encontrado;
	}




	public T get(int pos) 
	{
		Node nod = null;
		// Verifica si la posici�n ingresada se encuentre en el rango
		// >= 0 y < que el numero de elementos del la lista.
		if(pos>=0 && pos<cont)
		{
			// Consulta si la posicion es el inicio de la lista.
			if (pos == 0 && primero != null)
			{
				// Retorna el valor del inicio de la lista.
				return (T) primero.darObjeto();

			}
			else if(primero == null)
			{
				return null;
			}
			else
			{
				// Crea una copia de la lista.
				nod = primero;
				// Recorre la lista hasta la posici�n ingresada.
				for (int i = 0; i < pos; i++) {
					nod = nod.darSiguiente();
				}
				// Retorna el valor del nodo.
				return (T) nod.darObjeto();
			}
			// Crea una excepci�n de Posicion inexistente en la lista.
		} 
		else
		{
			System.out.println("La posicion esta fuera del rango admitido, el maximo rango es :"+ cont);
			return null;
		}

	}


	public boolean hasNext() 
	{
		// TODO Auto-generated method stub
		if(primero != null)
		{
			return actual.darSiguiente() == null ? false: true;
		}
		else
		{
			return false;
		}
	}

	public void set(int pos , T obj)
	{
		Node nod = new Node(obj, null);
	
		if(pos>=0 && pos<cont)
		{			
		if (pos == 0 && primero == null)
			{
				primero = nod;
			}
			else if(pos == 0 && primero != null)
			{
				Node tem = primero.darSiguiente();
				primero = nod;
				nod.cambiarSiguiente(tem);
				tem.cambiarAnterior(nod);
			}
			else if(pos != 0 && primero != null)
			{
				// Crea una copia de la lista.
				Node node = primero;
				// Recorre la lista hasta la posici�n ingresada.
				for (int i = 0; i < pos; i++)
				{
					node = node.darSiguiente();				
				}
				// Retorna el valor del nodo.
				if(node.darSiguiente()!=null && node.darAnterior()!=null)
				{
				Node anterior = node.darAnterior();
				anterior.cambiarSiguiente(nod); 
				nod.cambiarAnterior(anterior);
				nod.cambiarSiguiente(node.darSiguiente());
				node.darSiguiente().cambiarAnterior(nod);
				node = null;
				}
				else if(node.darSiguiente()==null && node.darAnterior()!=null)
				{
					node.darAnterior().cambiarSiguiente(nod); 
					nod.cambiarSiguiente(node.darSiguiente());				
					node = null;
				}
				else if(node.darSiguiente()!=null && node.darAnterior()==null)
				{
					nod.cambiarSiguiente(node.darSiguiente());
					node.darSiguiente().cambiarAnterior(nod);
					node = null;
				}
			}
			// Crea una excepci�n de Posicion inexistente en la lista.
		} 
		else
		{
			System.out.println("La posicion esta fuera del rango admitido, el maximo rango es :"+ cont);
			
		}
	}
	
	public void sumarLista(Lista<T> list)
	{
		ultimo.cambiarSiguiente(list.primero);
		ultimo.darSiguiente().cambiarAnterior(ultimo);
		ultimo = list.darUltimo();
		cont+=list.size();
		
	}

}





