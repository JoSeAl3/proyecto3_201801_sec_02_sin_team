package model.data_structures;

import model.data_structures.IArco;
import model.data_structures.IVertice;
public class Coordenada implements IVertice<String> 
{
	private double longitud;
	private double latitud;
	
	public  Coordenada(double lat, double lo) 
	{
		longitud = lo;
		latitud = lat;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	public double getLatitud() {
		return latitud;
	}
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	public String darId()
	{
		return latitud+"/"+longitud;
	}
	
}
