package model.data_structures;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.sun.jmx.snmp.Timestamp;
import model.vo.Service;

public class HashTreeDeServicios <K>
{

	private int n;           //cantidad de llaves en la tabla
	private int m;           //tama�o de la tabla
	private K[] llaves;      //llaves
	private RedBlackBST<Date, Service>[] valores;   	 // valores
	private static final int INI = 13; //constante de tam inicial


	public  HashTreeDeServicios()
	{
		this(INI);
	}
	
	public  HashTreeDeServicios(int capacidad)
	{
		m = capacidad;
		n = 0;
		llaves = (K[]) new Object[m];
		valores = new RedBlackBST[m];
	}

	public int size() {
		return m;
	}
	
	public int cantidadLaves()
	{
		return n;
	}
	public void put( K llave, Service valor)
	{
		
		String data = valor.getFecha();
		DateFormat formatter;
	    formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		
	    
		Date fecha = null;
		try 
		{
			fecha = formatter.parse(data);		
		} 
		catch (ParseException e)
		{
			// TODO Auto-generated catch block
			System.out.println("es un error");
			e.printStackTrace();
		}

		if(llave == null || valor == null)
		{
			System.out.println("Error la llave o el valor son null");
			System.out.println(llave);
			System.out.println(valor);
		}

		
		int i, cont;
		cont =0;
		for (i = hash(llave); llaves[i] != null&& cont< 10; i = ((i+1) % m) ) 
		{	

			if ((  llaves[i]).equals(llave)) 
			{				
				valores[i].put(fecha, valor);
				return;
			}
			cont++;
		}
		llaves[i] = llave;
		valores[i] = new RedBlackBST<>();
		valores[i].put(fecha, valor);
		n++;
		
	
		
		if(factorDeCarga()>=0.5)
		{
			
			reSize();
		}
	
	}
	
	public void put( K llave ,RedBlackBST arbol)
	{
		if(llave == null || arbol == null)
		{
			System.out.println("Error la llave o el valor son null");
			System.out.println(llave);
			System.out.println(arbol);
		}
		int i;
		for (i = hash(llave); llaves[i] != null; i = (i ++) % m) 
		{
			if ((  llaves[i]).equals(llave)) 
			{				
				valores[i]=arbol;
				return;
			}
		}		
		n++;
	}


	public RedBlackBST get(K llave)
	{
		for (int i = hash(llave); llaves[i] != null; i = ((i++) % m))
			if (llaves[i].equals(llave))
			{
				return valores[i];
			}
		return null;
	}


	public void delete(K llave)	
	{
//		if (!contains(llave)) return;
//
//		// busco la llave
//		int i = hash(llave);
//		while (!(llave.equals(llaves[i]))) 
//		{
//			i = (i + 1) % m;
//		}
//
//		// elimino los valores
//		llaves[i] = null;
//		valores[i] = null;
//		n--;
//		reHash(i);	
	}


	public int hash(K llave)
	{
		int x = 0;
		x = (llave.hashCode() & 0x7fffffff) % m;
	
		return x;

	}

	public double factorDeCarga() 
	{

		double numer = n;
		double deno = m;
		double f = numer/deno;		
		return f;

	}

	public void reHash(int pos )
	{
		// re-ordeno el arreglo desde la pos
		int i = (pos + 1) % m;
		while (llaves[i] != null) 
		{
			// eliminar y re agrgar todosa las llaves en el arreglo
			//guardo llaves y valores
			K  keyToRehash = llaves[i];
			RedBlackBST valToRehash = valores[i];
			//los elimino del arreglo
			llaves[i] = null;
			valores[i] = null;
			n--;
			// los re incerto
			put(keyToRehash, valToRehash);
			i = (i + 1) % m;
		}
	}

	public void reSize()
	{		
		HashTreeDeServicios<K> temp = new HashTreeDeServicios<K>(m*2);
		for (int i = 0; i < m; i++)
		{
			if (llaves[i] != null) 
			{
				temp.put(llaves[i], valores[i]);
			}
		}
		this.llaves = temp.getLlaves();
		this.valores = temp.getValores();
		m    = temp.getM();	
	}
	
	public boolean contains(K llave) {
		if (llave == null)
		{
			throw new IllegalArgumentException("llave null");

		}
		return get(llave) != null;
	}



	public Iterator<K> keys()	
	{
		return new iteradorDeStL<K>();
	}

	public class iteradorDeStL<K> implements Iterator<K>
	{

		private int cont = 0;

		public iteradorDeStL()
		{
						
		}
		
		public boolean hasNext() {
			// TODO Auto-generated method stub

			return cont < llaves.length;
		}
		
		public K next() 
		{			
			cont++;			
			return (K) llaves[cont];
			
		}

	}
	
	public K[] getLlaves()
	{
		return llaves;
	}
	
	public RedBlackBST[ ] getValores(){
		
		return valores;
	}
	
	public int getM() {
		return m;
	}

}
