package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Pila <T > implements Iterable<T>
{


	private Node nodo;
	private T elem;
	private Node Next;
	private Node end;
	private int size;

	public Pila() 
	{
		end = null;
		size = 0;
	}

	public void push(T pElemento) 
	{
		Node new_node = new Node(pElemento, null);
		if (end == null)
			end = new_node;
		else {
			new_node.cambiarSiguiente(end);
			end = new_node;
		}
		size++;
	}


	public T pop() 
	{
		if (end == null)
		{
			return null;
		}
		T elemento = (T) end.darObjeto();
		end = end.darSiguiente();
		size--;
		return elemento;
	}

	public boolean isEmpty() 
	{
		return end == null;
	}

	public int size() 
	{
		return size;
	}

	public T end() 
	{

		if (end == null)
			return null;
		else
			return (T) end.darObjeto();
	}


	public Iterator<T> iterator() 
	{
		return new ListIterator();
	}
	
	private class ListIterator implements Iterator<T> {
        private Node current = end;
        public boolean hasNext()  
        { 
        	return current != null;                  
        }
        public void remove()     
        { 
        	throw new UnsupportedOperationException();
        }

        public T next() {
            if (!hasNext())
            {
            	
            }
            T item = (T) current.darObjeto();
            current = current.darSiguiente(); 
            return item;
        }
    }


}
