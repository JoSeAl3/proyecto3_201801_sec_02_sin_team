package model.vo;

import model.data_structures.Lista;

public class InfoZonaRango {

//Atributos 
	
	private double zona;
	
	private RangoFechaHora rango; 
	
	private double plataRecibidaPick;
	
	private double plataRecibidaDrop;
	
	private double plataRecibidaBoth;
	
	private Lista<Service> serviciosDrop;
	private Lista<Service> serviciosPick;
	private Lista<Service> serviciosBoth;
	
	public InfoZonaRango(double pZona,RangoFechaHora pRango,double pPlataRecibidaPick,double pPlataRecibidaDrop,double pPlataRecibidaBoth ,Lista<Service> pServiciosDrop, Lista<Service> pServiciosPick, Lista<Service> pServiciosBoth)
	{
		zona = pZona;
		rango = pRango;
		plataRecibidaPick = pPlataRecibidaPick;
		plataRecibidaDrop =pPlataRecibidaDrop;
		plataRecibidaBoth = pPlataRecibidaBoth;
		serviciosDrop = pServiciosDrop;
		serviciosPick  = pServiciosPick;
		serviciosBoth = pServiciosBoth;
	}
		public double getZona()
		{
			return zona;
		}

		
		public RangoFechaHora getRango()
		{
			return rango;
		}

		public void setRango(RangoFechaHora rango)
		{
			this.rango = rango;
		}


		public double getPlataRecibidaPick() 
		{
			return plataRecibidaPick;
		}
		
		public double getPlataRecibidaDrop() 
		{
			return plataRecibidaDrop;
		}
		public double getPlataRecibidaBoth() 
		{
			return plataRecibidaBoth;
		}
		
		public int getServiciosDrop() 
		{
			return serviciosDrop.size();
		}
		
		public int getServiciosPick() 
		{
			return serviciosPick.size();
		}
		public int getServiciosBoth() 
		{
			return serviciosBoth.size();
		}


		



}
