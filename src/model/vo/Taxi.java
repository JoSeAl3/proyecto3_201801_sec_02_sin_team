package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	
	private String  compania;
	private String taxiId;
	private String fare;
	
	
	public Taxi (String pCompania, String pTaxId, String pFare)
	{
		compania = pCompania;
		taxiId = pTaxId;
		fare = pFare;
		
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return compania;
	}
	
	public Double getFare()
	{
		Double h = Double.parseDouble(fare);
		return h;
	}
	
	
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		int h = 0 ;
		
		h = this.taxiId.compareTo(o.taxiId);
		
		
		return h;
	}	
}
