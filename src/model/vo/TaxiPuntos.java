package model.vo;

public class TaxiPuntos implements Comparable<TaxiPuntos>
{
	private double puntos;

	private String taxid;

	public TaxiPuntos (String id, double points)
	{
		puntos = points;
		taxid = id;
	}

	public double getPuntos() {
		return puntos;
	}

	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}

	public String getTaxid() {
		return taxid;
	}

	public void setTaxid(String taxid) {
		this.taxid = taxid;
	}

	@Override
	public int compareTo(TaxiPuntos o) 
	{		
		return (int) (this.puntos - o.puntos);
	}


}
