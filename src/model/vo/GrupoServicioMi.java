package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.Lista;

public class GrupoServicioMi implements Comparable<GrupoServicioMi>
{
	private Lista<Service> arr;
	private int distancia;
	
	public GrupoServicioMi (int pDis)
	{
		distancia = pDis;	
		arr = new Lista() ;
	}
	
	public void agregar(Service ser)
	{
		arr.add(ser);

	}

	public Integer getDistancia()
	{
		
		return	distancia;
	}

	public Lista getServicios() {
		// TODO Auto-generated method stub
		return arr;
	}

	@Override
	public int compareTo(GrupoServicioMi o) 
	{
		return this.arr.size() - o.arr.size();
	}
}
