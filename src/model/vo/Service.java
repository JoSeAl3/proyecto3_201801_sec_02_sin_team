package model.vo;



import java.sql.Timestamp;

import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.Coordenada;
import model.data_structures.IArco;
/**
 * Representation of a Service object
 */
public class Service implements IArco {


	private Taxi tax;
	private String 	dropoff_census_tract;
	private String 	dropoff_centroid_latitude;
	private Coordenada	dropoff_centroid_location;
	private String	dropoff_centroid_longitude;
	private String 	dropoff_community_area;
	private String 	extras;
	private String 	fare;
	private String 	payment_type;
	private String	pickup_census_tract;
	private String 	pickup_centroid_latitude;
	private Coordenada	pickup_centroid_location;
	private String  pickup_centroid_longitude;
	private String	pickup_community_area;

	private String 	tips;
	private String  tolls;
	private String  trip_end_timestamp;
	private String  trip_id;
	private String trip_miles;
	private String  trip_seconds;
	private String	trip_start_timestamp;
	private String 	trip_total;
	private String  Trip_Id;
	


	public Service(String pcompany,  String pdropoff_census_tract, String pdropoff_centroid_latitude,
			Coordenada fin, String pdropoff_centroid_longitude, String pdropoff_community_area,
			String pextras, String pfare, String pPayment_type, String pPickup_census_tract,
			String pPickup_centroid_latitude, Coordenada ini, String pPickup_centroid_longitude,
			String pPickup_community_area, String ptaxi_id, String ptips, String ptolls, String ptrip_end_timestamp,
			String ptrip_id, String ptrip_miles, String ptrip_seconds, String ptrip_start_timestamp, String ptrip_total) {


		dropoff_census_tract = pdropoff_census_tract;
		dropoff_centroid_latitude = pdropoff_centroid_latitude;
		dropoff_centroid_location =  fin;
		dropoff_centroid_longitude = pdropoff_centroid_longitude;
		dropoff_community_area = pdropoff_community_area;
		extras = pextras;
		fare = pfare;
		payment_type = pPayment_type;
		pickup_census_tract = pPickup_census_tract;
		pickup_centroid_latitude = pPickup_centroid_latitude;
		pickup_centroid_location = ini;
		pickup_centroid_longitude = pPickup_centroid_longitude;
		pickup_community_area = pPickup_community_area;
		tips = ptips;
		tolls = ptolls;
		trip_end_timestamp = ptrip_end_timestamp;
		trip_id = ptrip_id;
		trip_miles = ptrip_miles;
		trip_seconds = ptrip_seconds;
		trip_start_timestamp = ptrip_start_timestamp;
		trip_total = ptrip_total;
		tax = new Taxi (pcompany , ptaxi_id , fare);
	}


	public double getDropoff_centroid_latitude() {
		return Double.parseDouble(dropoff_centroid_latitude);
	}


	public void setDropoff_centroid_latitude(String dropoff_centroid_latitude) {
		this.dropoff_centroid_latitude = dropoff_centroid_latitude;
	}


	public double getDropoff_centroid_longitude() {
		return Double.parseDouble(dropoff_centroid_longitude);
	}


	public void setDropoff_centroid_longitude(String dropoff_centroid_longitude) {
		this.dropoff_centroid_longitude = dropoff_centroid_longitude;
	}


	public double getPickup_centroid_latitude()
	{
		return Double.parseDouble(pickup_centroid_latitude);
	}


	public String getTolls() {
		return tolls;
	}


	public void setTolls(String tolls) {
		this.tolls = tolls;
	}


	public void setPickup_centroid_latitude(String pickup_centroid_latitude) {
		this.pickup_centroid_latitude = pickup_centroid_latitude;
	}


	public double getPickup_centroid_longitude() 
	{
		return Double.parseDouble(pickup_centroid_longitude);
	}


	public void setPickup_centroid_longitude(String pickup_centroid_longitude) {
		this.pickup_centroid_longitude = pickup_centroid_longitude;
	}


	public Coordenada getDropoff_centroid_location() 
	{
		return dropoff_centroid_location;
	}


	public void setDropoff_centroid_location(Coordenada dropoff_centroid_location) {
		this.dropoff_centroid_location = dropoff_centroid_location;
	}


	public Coordenada getPickup_centroid_location() {
		return pickup_centroid_location;
	}


	public void setPickup_centroid_location(Coordenada pickup_centroid_location) 
	{
		this.pickup_centroid_location = pickup_centroid_location;
	}


	/**
	 * @return id - Trip_id
	 */
	public String getTripId() 
	{
		// TODO Auto-generated method stub

		return trip_id;
	}	
	
	public String getTripStart() 
	{

		return trip_start_timestamp;
	}	
	
	public String getTripEnd() 
	{

		return trip_end_timestamp;
	}	


	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		return tax.getTaxiId();
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		int i = Integer.parseInt(trip_seconds);
		return i;
	}



	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		double d = Double.parseDouble(trip_miles);
		return d;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		double d = Double.parseDouble(trip_total);
		return d;
	}

	public Taxi getTaxi() {
		return tax;
	}

	public int getArea() 
	{
		int i = Integer.parseInt(dropoff_community_area);
		return i;
	}
	public int getDropoffZone() 
	{
		int i = Integer.parseInt(dropoff_community_area);
		return i;
	}
	
	public int getPickuZone() 
	{
		int i = Integer.parseInt(pickup_community_area);
		return i;
	}
	
	public Double getFare()
	{
		Double h = Double.parseDouble(fare);
		return h;
	}
	

	
	
	public String getFecha()
	{
		return  trip_start_timestamp;
	}


	public RangoFechaHora getRango()
	{
		String[] fechai = trip_end_timestamp.split("T");
		String[] fechaf = trip_start_timestamp.split("T");	
		RangoFechaHora rango = new RangoFechaHora(fechai[0], fechaf[0], fechai[1], fechaf[1]);
		return rango;
	}


	@Override
	public int darPeso() 
	{
		
		return Integer.parseInt(trip_total);
	}
	

}
