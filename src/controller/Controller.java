package controller;


import java.text.ParseException;

import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.RedBlackBST;
import model.data_structures.hashTableLinearProbing;
import model.data_structures.hashTableSeparateChaining;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.GrupoServicioMi;
import model.vo.InfoTaxiRango;
import model.vo.InfoZonaRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.TaxiPuntos;
import model.vo.ZonaServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static TaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean cargarSistema(String direccionJson)
	{
		try {
			return manager.cargarSistema(direccionJson);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	 


	//1B
	public static	String darInfoArbol()
	{
		return manager.arbolInfo();
		
	}
	
	//2B
	public static InfoTaxiRango infoTaxi(String a) 
	{
		return manager.darInfo(a);
	}	
	
	//3C
	public static Cola<Service> idsEnRango(String fecha, String hora)
	{
		return manager.darIdsRango( fecha,  hora);
	}

}
