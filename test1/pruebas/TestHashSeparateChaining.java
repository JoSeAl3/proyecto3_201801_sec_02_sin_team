package pruebas;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;


import model.data_structures.hashTableSeparateChaining;

public class TestHashSeparateChaining 
{
	private hashTableSeparateChaining<Integer, Integer> st;
	@Before
	public void setUpEscenario1() 
	{
		st = new hashTableSeparateChaining<Integer, Integer>();
	}

	public void setUpEscenario2() 
	{
		st = new hashTableSeparateChaining<Integer, Integer>( );
		Integer a = new Integer(1);
		st.put(a, a);
	}

	public void setUpEscenario3() 
	{
		st = new hashTableSeparateChaining <Integer, Integer>( );
		//agrego numeros del 0  al  24
		for (Integer i = 0 ; i < 25; i++)
		{
			st.put(i, i);		
		}
	}

	@Test
	public void  hashTableLinearProbingTest()
	{
		assertEquals("El n�mero de elementos no es el correcto", st.cantidadLaves() , 0);
	}

	@Test
	public void testPut()
	{
		Integer a = new Integer(1);
		st.put(a, a+10);
		assertEquals("no agrega en vacio",   st.cantidadLaves() , 1 );
		setUpEscenario2();
		st.put(11,110);
		assertEquals("El n�mero de elementos no es el correcto", st.cantidadLaves() , 2);
		setUpEscenario3();
		st.put(30,110);		
		assertEquals("El n�mero de elementos no es el correcto", st.cantidadLaves() , 26);
		
		int initial = st.get(16);
		st.put(16, 0);
		int fial = st.get(16);
		System.out.println("-----------------"+fial);
		assertNotEquals("el valor deve cambiar", fial , initial);
		assertEquals("el valor es ", fial , 0);
		}

	@Test
	public void testDelete() 
	{
		st.delete(1);
		assertEquals("El n�mero de elementos no es el correcto",st.cantidadLaves() , 0 );
		setUpEscenario2();
		st.delete(1);		
		assertEquals("El n�mero de elementos no es el correcto",st.cantidadLaves() , 0 );
		setUpEscenario3();
		st.delete(5);
		assertEquals("El n�mero de elementos no es el correcto",st.cantidadLaves() , 24);
	}	

	@Test
	public void testGet() 
	{
		assertNull(st.get(1));
		setUpEscenario2();
		int a = st.get(1);		
//		setUpEscenario3();
//		int b = 0;
//		Iterator iter = st.keys();
//		while(iter.hasNext()&& b< 10)
//		{		
//			System.out.println(b+"");
//			int c = st.get(b);
//			assertEquals("El n�mero de elementos no es el correcto",c , b);
//			b++;
//			st.keys().next();
//		}
	}
	@Test
    public void testContains()
    {

        testPut();

        assertEquals(true, st.contains(2));



    }

	@Test
	public void testSize() 
	{
		assertEquals("Deberia encontrar el primer elemento", 4, st.size());
		setUpEscenario2();
		assertEquals("Deberia encontrar el primer elemento",	4, st.size());
		setUpEscenario3();
		assertEquals("Deberia encontrar el primer elemento", 8, st.size());
	}
}
