package pruebas;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Pila;




public class PilaTest 
{

	private Pila<String> p = new Pila<String>();
	
	
	@Before
    public void setupEscenario1( )
    {
		p = new Pila<String>();
		p.push ("hola");
    }
    
    public void setupEscenario2()
    {
    	p = new Pila<String>();
    	
    }
	
	@Test
	public void testPush() 
	{	
		
		p.push("Hi");
		assertTrue( "El dibujo se cre� incorrectamente", p.pop().equals("Hi") );
	}
	
	@Test
	public void testPop() {
		
	
		assertEquals( "El dibujo se cre� incorrectamente", p.pop(), "hola" );
	}
	
	@Test
	public void testIsEmpty() {
		setupEscenario2( );
		assertNull( "El dibujo se cre� incorrectamente", p.pop() );
	}
	
	@Test
	public void testSize() {
	
		assertEquals( "El dibujo se cre� incorrectamente", 1 ,p.size() );
	}
	
	@Test
	public void testEnd() {
		
		
		assertTrue( "El dibujo se cre� incorrectamente", p.end().equals("hola") );
	}

}
